package de.skatulator.preference;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import de.skatulator.R;
import de.skatulator.util.Calculator;

public class ConversionRateDialogPreference extends DialogPreference {

    private EditText conversionRateEditText;

    public ConversionRateDialogPreference(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setDialogLayoutResource(R.layout.dialog_conversionrate);
    }

    @Override
    protected void onBindDialogView(View view) {
        super.onBindDialogView(view);
        conversionRateEditText = view.findViewById(R.id.dialog_conversionrate_edittext);
        conversionRateEditText.setText(getSharedPreferences().getString(getContext().getString(R.string.rechner_settings_conversion_rate_key), "1.0"));
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);
        if(positiveResult) {
            SharedPreferences.Editor editor = getEditor();
            // Set conversion rate to 1.0 if nothing or zero was entered, set to entered value otherwise
            editor.putString(getContext().getString(R.string.rechner_settings_conversion_rate_key), String.valueOf(Calculator.parseDouble(conversionRateEditText.getText().toString())));
            editor.apply();
        }
    }
}
