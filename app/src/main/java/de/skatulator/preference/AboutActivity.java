package de.skatulator.preference;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.method.LinkMovementMethod;
import android.widget.LinearLayout;
import android.widget.TextView;

import de.skatulator.R;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        setTitle(R.string.about_title);
        LinearLayout root = findViewById(R.id.about_layout);
        root.setBackgroundColor(ContextCompat.getColor(this, android.R.color.background_light));
        // Make bug report link clickable
        TextView helpLink = findViewById(R.id.about_bugslink);
        helpLink.setMovementMethod(LinkMovementMethod.getInstance());
        // Make source link clickable
        TextView sourceLink = findViewById(R.id.about_sourcelink);
        sourceLink.setMovementMethod(LinkMovementMethod.getInstance());
    }
}
