package de.skatulator;

import android.content.Intent;
import android.graphics.Canvas;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import de.skatulator.adapter.RechnerAdapter;
import de.skatulator.preference.PreferencesActivity;
import de.skatulator.viewholder.PlayerCardViewHolder;

// Main fragment with list of players
public class RechnerFragment extends Fragment {

    private RechnerAdapter rechnerAdapter;

    public RechnerFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        rechnerAdapter = new RechnerAdapter(getContext());
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view =  inflater.inflate(R.layout.fragment_rechner, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.recycler);
        recyclerView.setAdapter(rechnerAdapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        // Swipe to delete
        ItemTouchHelper.SimpleCallback swipeCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.END) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                if (direction == ItemTouchHelper.END) {
                    PlayerCardViewHolder playerCardViewHolder = (PlayerCardViewHolder) viewHolder;
                    rechnerAdapter.removePlayer(playerCardViewHolder.getPlayerNumber());

                }
            }

            // Animate swipe to delete by decreasing opacity of the card according to the horizontal position
            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                // Fade out on swipe
                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
                    viewHolder.itemView.setAlpha(1.0f - Math.abs(dX) / viewHolder.itemView.getWidth());
                    viewHolder.itemView.setTranslationX(dX);
                } else {
                    super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
                }
            }
        };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(swipeCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        super.onCreateOptionsMenu(menu, menuInflater);
        menuInflater.inflate(R.menu.menu_rechner, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            // launch settings activity
            startActivity(new Intent(getActivity(), PreferencesActivity.class));
            return true;
        }
        else if (id == R.id.action_add_player) {
            // add player to list
            rechnerAdapter.addPlayer();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        rechnerAdapter.pause();
    }

    @Override
    public void onResume() {
        super.onResume();
        // restore last list of players when starting/returning to app
        rechnerAdapter.resume();
    }
}
