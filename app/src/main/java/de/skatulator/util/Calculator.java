package de.skatulator.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.HashSet;

import de.skatulator.R;

public class Calculator {

    private static boolean wFactor;
    private static double conversionRate;
    private static boolean settingsInitialized = false;
    private static final String LOG_TAG = "Calculator";

    /* Custom parser that returns zero if there was en error exporting player data and the number
       for player points is invalid */
    public static int parseInt(String s) {
        int i;
        try{
            i = Integer.parseInt(s);
        }
        catch(NumberFormatException e) {
            i = 0;
        }
        return i;
    }

    // Custom parser for conversion rate
    public static double parseDouble(String s) {
        double d;
        try{
            d = Double.parseDouble(s);
            if (d == 0) d = 1.0;
        }
        catch(NumberFormatException e) {
            d = 1.0;
        }
        return d;
    }

    public static double getMoney(int playerNumber) {
        PlayerList playerList = PlayerList.getInstance();
        HashSet<Player> players = playerList.getPlayers();
        int totalPoints = 0;
        Player currPlayer = null;
        double money = 0;
        // Get total points and player for the number provided
        for (Player p : players) {
            totalPoints += p.getPoints();
            if (p.getNumber() == playerNumber) currPlayer = p;
        }

        if (currPlayer != null) {
            //Need to multiply by 0.01 to get actual currency values instead of cents
            money = 0.01 * conversionRate * (players.size() * currPlayer.getPoints() - totalPoints);
        }
        if (!settingsInitialized) Log.w(LOG_TAG, "Calculations settings have not been initialized yet. Calculated money is likely wrong. Call updateCalcSettings before calculating money.");
        // Scale money if using W-factor
        double actMoney = (wFactor ? money / (players.size() - 1) : money);
        Log.v(LOG_TAG, "Player " + playerNumber + " wins/loses " + actMoney);
        return actMoney;
    }

    // Format number as money with the local currency
    public static String formatMoney(double d) {
        NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();
        currencyFormat.setRoundingMode(RoundingMode.HALF_UP);
        return currencyFormat.format(d);
    }

    // Get W-factor and conversion rate
    public static void updateCalcSettings(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        conversionRate = parseDouble(preferences.getString(context.getString(R.string.rechner_settings_conversion_rate_key), "1"));
        wFactor = preferences.getBoolean(context.getString(R.string.rechner_settings_wfactor_key), true);
        settingsInitialized = true;
    }
}
