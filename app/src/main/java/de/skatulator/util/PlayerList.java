package de.skatulator.util;

import android.util.Log;

import java.util.HashSet;

// List of players, singleton to avoid consistency problems
public class PlayerList {

    private static final String LOG_TAG = "PlayerList";

    // SINGLETON

    private static class PlayerListHolder {
        private static final PlayerList INSTANCE = new PlayerList();
    }

    private PlayerList() {}

    public static PlayerList getInstance() {
        return PlayerListHolder.INSTANCE;
    }

    // ---

    private HashSet<Player> players = new HashSet<>();

    public HashSet<Player> getPlayers() {
        return players;
    }

    // Add a new player
    public void addPlayer() {
        int maxNumber = 0;
        for (Player p : players) {
            if (p.getNumber() > maxNumber) maxNumber = p.getNumber();
        }
        players.add(new Player(maxNumber + 1, 0));
    }

    // Add an already existing player
    public void addPlayer(Player player) {
        players.add(player);
    }

    // Replace player
    public void setPlayer(int playerNumber, int points) {
        for (Player p : players) {
            if (p.getNumber() == playerNumber) {
                p.setPoints(points);
            }
        }
    }

    public void removePlayer(int playerNumber) {
        if (playerNumber == players.size()) {
            for (Player p : players) {
                if (p.getNumber() == playerNumber) {
                    Log.v(LOG_TAG, "Removing last player");
                    players.remove(p);
                    break;
                }
            }
        } else {
            boolean playerExists = false;
            for (Player p : players) {
                if (p.getNumber() == playerNumber) {
                    players.remove(p);
                    playerExists = true;
                    break;
                }
            }
            if (playerExists) {
                for (Player p : players) {
                    if (p.getNumber() > playerNumber) {
                        p.setNumber(p.getNumber() - 1);
                    }
                }
            } else {
                Log.w(LOG_TAG, "Player with number " + playerNumber + " does not exist. Could not delete.");
            }

        }
    }

    public Player getPlayer(int playerNumber) {
        for (Player p : players) {
            if (p.getNumber() == playerNumber) return p;
        }
        Log.w(LOG_TAG, "Could not find player number " + playerNumber);
        return null;
    }

    public int getNumberOfPlayers() {
        return players.size();
    }

    public void resetPlayers() {
        players.clear();
        for (int i = 1; i < 4; i++) {
            Player p = new Player(i, 0);
            players.add(p);
        }
    }

    public String exportPoints() {
        String pointsExport = "";
        for (int i = 1; i <= players.size(); i++) {
            for (Player p : players) {
                if (p.getNumber() == i) {
                    pointsExport += String.valueOf(p.getPoints());
                    if (i != players.size()) pointsExport += ",";
                    break;
                }
            }
        }
        Log.v(LOG_TAG, "Export " + players.size() + " players w. " + pointsExport + " points");
        return pointsExport;
    }

    public void importPoints(String pointsImport) {
        if (pointsImport.equals("")) resetPlayers();
        else {
            String pointsSplit[] = pointsImport.split(",");
            players.clear();
            for (int i = 0; i < pointsSplit.length; i++) {
                Player p = new Player(i + 1, Calculator.parseInt(pointsSplit[i]));
                players.add(p);
            }
        }
    }

}
