package de.skatulator.util;

// Player object with number and points
public class Player {

    private int number;
    private int points;

    public Player(int number, int points) {
        this.number = number;
        this.points = points;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }
}
