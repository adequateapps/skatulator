package de.skatulator.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.HashSet;
import java.util.Set;

import de.skatulator.R;
import de.skatulator.util.Calculator;
import de.skatulator.util.Player;
import de.skatulator.util.PlayerList;
import de.skatulator.viewholder.PlayerCardViewHolder;

public class RechnerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context mContext;
    private final static String LOG_TAG = "RechnerAdapter";

    /* All money views need to be changed immediately when any EditText changes and this has to
    happen without deleting the current cards and the EditText being edited with it. The solution is
    to keep a reference to all bound ViewHolders and updating their EditText views through this
    reference. ViewHolders are added in onBindViewHolder and removed in onViewRecycled.
     */
    private Set<PlayerCardViewHolder> boundViewHolders = new HashSet<>();

    public RechnerAdapter(Context context) {
        mContext = context;
        // Reset players to default
        PlayerList playerList = PlayerList.getInstance();
        playerList.resetPlayers();
        // Get W-factor and conversion rate
        Calculator.updateCalcSettings(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return PlayerCardViewHolder.newInstance(mContext, LayoutInflater.from(parent.getContext()).inflate(R.layout.rechner_card, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        PlayerCardViewHolder holder = (PlayerCardViewHolder) viewHolder;
        PlayerList playerList = PlayerList.getInstance();
        for (Player p : playerList.getPlayers()) {
            if (p.getNumber() == position + 1) {
                holder.initializeCard(this, p.getNumber());
            }
        }
        boundViewHolders.add(holder);
    }

    @Override
    public void onViewRecycled(RecyclerView.ViewHolder holder) {
        PlayerCardViewHolder playerCardViewHolder = (PlayerCardViewHolder) holder;
        Log.v(LOG_TAG, "VH of player " + playerCardViewHolder.getPlayerNumber() + " recycled");
        boundViewHolders.remove(holder);
    }

    @Override
    public int getItemCount() {
        PlayerList playerList = PlayerList.getInstance();
        return playerList.getPlayers().size();
    }

    public void addPlayer() {
        PlayerList playerList = PlayerList.getInstance();
        playerList.addPlayer();
        notifyItemInserted(getItemCount() - 1);
        updateViews(false, 0);
    }

    public void removePlayer(int playerNumber) {
        PlayerList playerList = PlayerList.getInstance();
        playerList.removePlayer(playerNumber);
        notifyItemRemoved(playerNumber - 1);
        if (playerNumber != getItemCount()) notifyItemRangeChanged(playerNumber, getItemCount() - playerNumber);
        updateViews(true, playerNumber);
        // Need at least 3 players
        if (playerList.getNumberOfPlayers() < 3) addPlayer();
    }

    /* Update cards in adapter, two call scenarios:
       1. Update money after number of players or points change
          --> arguments: false, <any value>
       2. Update money + player no after a player (that was not at end of list) was deleted
          --> arguments: true, number of the player that was deleted */
    public void updateViews(boolean afterDelete, int playerNumber) {
        for (PlayerCardViewHolder holder : boundViewHolders) {
            Log.v(LOG_TAG, "Updating " + (afterDelete ? "all data" : "money") + " in VH of player " + holder.getPlayerNumber());
            if (afterDelete) holder.updateData(playerNumber);
            else holder.updateMoney();
        }
    }

    // store current list of players and points when the main fragment gets paused
    public void pause() {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(mContext).edit();
        PlayerList playerList = PlayerList.getInstance();
        editor.putString(mContext.getString(R.string.rechner_data_points_key), playerList.exportPoints());
        editor.apply();
    }

    // restore last list of players and points when the main fragment resumes
    public void resume() {
        Calculator.updateCalcSettings(mContext);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        String pointsImport = preferences.getString(mContext.getString(R.string.rechner_data_points_key), "");
        PlayerList playerList = PlayerList.getInstance();
        playerList.importPoints(pointsImport);
        notifyDataSetChanged();
    }
}
