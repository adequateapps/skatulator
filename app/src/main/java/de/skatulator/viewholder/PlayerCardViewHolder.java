package de.skatulator.viewholder;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import de.skatulator.R;
import de.skatulator.adapter.RechnerAdapter;
import de.skatulator.util.Calculator;
import de.skatulator.util.PlayerList;

public class PlayerCardViewHolder extends RecyclerView.ViewHolder implements TextWatcher {

    private Context context;
    private RechnerAdapter adapter;
    private int playerNumber;
    private TextView playerTextView;
    private TextView winLoseTextView;
    private EditText pointsEditText;
    private TextView moneyTextView;

    private PlayerCardViewHolder(Context context, final View parent) {
        super(parent);
        this.context = context;
        playerTextView = parent.findViewById(R.id.player_no);
        winLoseTextView = parent.findViewById(R.id.player_winlose_text);
        pointsEditText = parent.findViewById(R.id.player_points_text);
        moneyTextView = parent.findViewById(R.id.player_money_text);
    }

    public static PlayerCardViewHolder newInstance(Context context, View parent) {
        return new PlayerCardViewHolder(context, parent);
    }

    public void initializeCard(final RechnerAdapter adapter, int playerNumber) {
        this.adapter = adapter;
        this.playerNumber = playerNumber;
        PlayerList playerList = PlayerList.getInstance();
        pointsEditText.setText(String.valueOf(playerList.getPlayer(playerNumber).getPoints()));
        playerTextView.setText(context.getString(R.string.player_no_text, playerNumber));
        updateMoney();

        pointsEditText.addTextChangedListener(this);

    }
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        // Do nothing
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        // Do nothing
    }

    @Override
    public void afterTextChanged(Editable s) {
        PlayerList playerList = PlayerList.getInstance();
        playerList.setPlayer(playerNumber, Calculator.parseInt(s.toString()));
        adapter.updateViews(false, 0);
    }

    // Update all data after player was deleted, need to change player number accordingly
    public void updateData(int playerNumber) {
        if (this.playerNumber > playerNumber) {
            this.playerNumber--;
            playerTextView.setText(context.getString(R.string.player_no_text, playerNumber));
        }
        updateMoney();
    }

    public void updateMoney() {
        double money = Calculator.getMoney(playerNumber);
        moneyTextView.setText(Calculator.formatMoney(money));

        if (money < 0) winLoseTextView.setText(context.getString(R.string.player_winlose_text_lose));
        else winLoseTextView.setText(context.getString(R.string.player_winlose_text_win));

        int moneyColor;
        if (money < -0.005) moneyColor = ContextCompat.getColor(context, R.color.colorLose);
        else if (money > 0.005) moneyColor = ContextCompat.getColor(context, R.color.colorWin);
        else moneyColor = ContextCompat.getColor(context, R.color.colorGrey);

        winLoseTextView.setTextColor(moneyColor);
    }

    public int getPlayerNumber() {
        return playerNumber;
    }

}
