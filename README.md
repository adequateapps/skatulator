# Skatulator - the Pfennigskat calculator for Android devices

Skatulator is a free and open source app for calculating wins and losses at the end of a Pfennigskat session. It supports any amount of players and you can choose the conversion rate between money and points in the game.

## Features

### W-factor

The W-factor, named after a pioneer in Pfennigskat calculating technology, scales the money won by the number of opponents. Without it, winners get their winning amount from each of the players while with it, the wins are divided up between the losers. You can see a simple example here:

| Player | Points | Wins without W-factor | Wins with W-factor|
|:-------|:-------|:---------------- |:-------------|
| 1 | 96 | $1.92 | $0.96 |
| 2 | 0 | -$0.96 | -$0.48 |
| 3 | 0 | -$0.96 | -$0.48 |

This example assumes 1 point = 1 cent.

## Contribute

You can contribute to this app by [reporting bugs.](https://bitbucket.org/adequateapps/skatulator/issues) You can also check out the [open issues](https://bitbucket.org/adequateapps/skatulator/issues?status=new&status=open) and create a pull request if you find a solution for any of the issues.

## License

This app and its source code are licensed under the MIT license.

Copyright 2017 Adequate Apps

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.